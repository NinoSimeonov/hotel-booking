import Room, { RoomAttributes } from '../models/room';
import Booking from '../models/booking';

export const createRoomService = async (roomData: RoomAttributes): Promise<Room> => {
  return Room.create(roomData);
};

export const getRoomsService = async (): Promise<Room[]> => {
  return Room.findAll();
};

export const getRoomService = async (id: number): Promise<Room | undefined> => {
  return Room.findById(id);
};

export const updateRoomService = async (id: number, updateData: Partial<RoomAttributes>): Promise<Room | undefined> => {
  return Room.updateById(id, updateData);
};

export const deleteRoomService = async (id: number): Promise<boolean> => {
  return Room.deleteById(id);
};

export const getAvailableRoomsService = async (startDate: string, endDate: string): Promise<Room[]> => {
  const allRooms = Room.findAll();
  const bookings = Booking.findAll();

  const searchStart = new Date(startDate);
  searchStart.setUTCHours(0, 0, 0, 0); // Use UTC time for consistency

  const searchEnd = new Date(endDate);
  searchEnd.setUTCHours(23, 59, 59, 999); // Use UTC time for consistency

  // Logic to filter out booked rooms
  const availableRooms = allRooms.filter(room => {
    return !bookings.some(booking => {
      const bookingStart = new Date(booking.date);
      const bookingEnd = new Date(bookingStart);
      bookingEnd.setUTCDate(bookingEnd.getUTCDate() + booking.nights);

      // Room is considered unavailable if the search range intersects with the booking range
      return booking.roomId === room.id && searchStart < bookingEnd && searchEnd > bookingStart;
    });
  });

  return availableRooms;
};

export const findBestDealRoom = async (date: string, nights: number, guests: number): Promise<Room | null> => {
  const availableRooms = await getAvailableRoomsService(date, nights.toString());
  // Filter for rooms that can accommodate the number of guests
  const suitableRooms = availableRooms.filter(room => {
    const totalBeds = room.singleBeds + room.doubleBeds * 2;
    return totalBeds >= guests;
  });

  // If there are no suitable rooms, return null
  if (suitableRooms.length === 0) return null;

  // Find the best deal room, assuming the cheapest room is the best deal
  const bestDealRoom = suitableRooms.reduce((best, room) => {
    if (!best || room.price < best.price) {
      return room;
    }
    return best;
  }, null as Room | null);

  return bestDealRoom;
};

export const getPaginatedAvailableRooms = async (
  startDate: string,
  endDate: string,
  page: number,
  limit: number

): Promise<{ data: Room[]; total: number }> => {
  const availableRooms = await getAvailableRoomsService(startDate, endDate);
  const totalRooms = availableRooms.length;

  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;
  const paginatedRooms = availableRooms.slice(startIndex, endIndex);

  return {
    data: paginatedRooms,
    total: totalRooms,
  };
};

export const getFilteredRoomsService = async (query: any): Promise<Room[]> => {
  const allRooms = Room.findAll();

  // Setup the filters object to include price, singleBeds, and doubleBeds
  const filters = {
    price: query.price ? Number(query.price) : undefined,
    singleBeds: query.singleBeds ? Number(query.singleBeds) : undefined,
    doubleBeds: query.doubleBeds ? Number(query.doubleBeds) : undefined,
  };

  // Filter rooms based on exact matches
  let filteredRooms = allRooms.filter(room => {
    return (!filters.price || room.price === filters.price) &&
           (!filters.singleBeds || room.singleBeds === filters.singleBeds) &&
           (!filters.doubleBeds || room.doubleBeds === filters.doubleBeds);
  });

  // Sorting logic based on query parameters
  if (query.sortBy) {
    const direction = query.sortOrder && query.sortOrder === 'desc' ? -1 : 1; // Default sort order is ascending
    filteredRooms.sort((a, b) => {
      switch (query.sortBy) {
        case 'price':
          return (a.price - b.price) * direction;
        case 'singleBeds':
          return (a.singleBeds - b.singleBeds) * direction;
        case 'doubleBeds':
          return (a.doubleBeds - b.doubleBeds) * direction;
        default:
          return 0;
      }
    });
  }

  return filteredRooms;
};
