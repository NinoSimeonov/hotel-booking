import Booking, { BookingAttributes } from '../models/booking';
import Room from '../models/room'; 
import { ERROR_MESSAGES } from '../utils/constants';

export const createBookingService = async (bookingData: BookingAttributes): Promise<Booking | string> => {
  const room = Room.findById(bookingData.roomId);

  // Check if the room exists
  if (!room) {
    return ERROR_MESSAGES.ROOM_NOT_FOUND;
  }

  // Check if the number of guests fits within the available beds
  const totalBeds = room.singleBeds + (2 * room.doubleBeds);
  if (bookingData.guests > totalBeds) {
    return `Insufficient beds for the number of guests. Available beds: ${totalBeds}`;
  }

  // Check if the room is available for the given date and number of nights
  if (!Booking.isRoomAvailable(bookingData.roomId, bookingData.date, bookingData.nights)) {
    return ERROR_MESSAGES.ROOM_NOT_AVAILABLE;
  }

  // If room is available, create the booking
  const newBooking = Booking.create(bookingData);
  if (newBooking) {
    return newBooking;
  } else {
    return ERROR_MESSAGES.UNABLE_TO_CREATE_BOOKING;
  }
};

export const getBookingsService = async (): Promise<Booking[]> => {
  return Booking.findAll();
};

export const getBookingService = async (id: number): Promise<Booking | undefined> => {
  return Booking.findById(id);
};

export const deleteBookingService = async (id: number): Promise<boolean> => {
  return Booking.deleteById(id);
};

export const updateBookingService = async (id: number, updateData: Partial<BookingAttributes>): Promise<Booking | string> => {
  const existingBooking = Booking.findById(id);
  if (!existingBooking) {
    return ERROR_MESSAGES.BOOKING_NOT_FOUND;
  }

  // Combine existing booking data with the update data
  const combinedData: BookingAttributes = { ...existingBooking, ...updateData };

  // Check if the room exists
  const room = Room.findById(combinedData.roomId);
  if (!room) {
    return ERROR_MESSAGES.ROOM_NOT_FOUND;
  }

  // Check if the number of guests fits within the available beds
  const totalBeds = room.singleBeds + (2 * room.doubleBeds);
  if (combinedData.guests > totalBeds) {
    return `Insufficient beds for the number of guests. Available beds: ${totalBeds}`;
  }

  // Check if the room is available for the updated date and number of nights
  if (!Booking.isRoomAvailable(combinedData.roomId, combinedData.date, combinedData.nights)) {
    return ERROR_MESSAGES.ROOM_NOT_AVAILABLE;
  }

  // Perform the update
  const updatedBooking = Booking.updateById(id, updateData);
  if (updatedBooking) {
    return updatedBooking;
  } else {
    return ERROR_MESSAGES.UNABLE_TO_UPDATE_BOOKING;
  }
};

export const getPaginatedBookingsService = async (page: number, limit: number): Promise<{ data: Booking[], total: number }> => {
  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;

  const paginatedBookings = Booking.findAll().slice(startIndex, endIndex);
  const totalBookings = Booking.findAll().length;

  return {
    data: paginatedBookings,
    total: totalBookings
  };
};