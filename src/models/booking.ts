export interface BookingAttributes {
  id?: number;
  roomId: number;
  date: string;
  guests: number;
  nights: number;
}

class Booking {
  static bookings: Booking[] = [];
  static nextId: number = 1;

  id: number;
  roomId: number;
  date: string;
  guests: number;
  nights: number;

  constructor({ id = Booking.nextId++, roomId, date, guests, nights }: BookingAttributes) {
      this.id = id;
      this.roomId = roomId;
      this.date = date;
      this.guests = guests;
      this.nights = nights;
  }

  static findAll(): Booking[] {
      return this.bookings;
  }

  static findById(id: number): Booking | undefined {
      return this.bookings.find(booking => booking.id === id);
  }
    // Modify your create method to check for room availability
    static create(bookingData: BookingAttributes): Booking | null {
        if (this.isRoomAvailable(bookingData.roomId, bookingData.date, bookingData.nights)) {
          // Room is available, proceed with creation
          const newBooking = new Booking(bookingData);
          this.bookings.push(newBooking);
          return newBooking;
        } else {
          // Room is not available
          return null;
        }
      }

  static updateById(id: number, updateData: Partial<BookingAttributes>): Booking | undefined {
      let booking = this.findById(id);
      if (!booking) return undefined;
      booking = { ...booking, ...updateData };
      this.bookings = this.bookings.map(b => (b.id === id ? booking : b));
      return booking;
  }

  static deleteById(id: number): boolean {
      const bookingIndex = this.bookings.findIndex(booking => booking.id === id);
      if (bookingIndex === -1) return false;
      this.bookings.splice(bookingIndex, 1);
      return true;
  }

  static isRoomAvailable(roomId: number, startDate: string, nights: number): boolean {
    const start = new Date(startDate);
    const end = new Date(start);
    end.setDate(end.getDate() + nights);

    return !this.bookings.some(booking => {
      const bookedStart = new Date(booking.date);
      const bookedEnd = new Date(bookedStart);
      bookedEnd.setDate(bookedEnd.getDate() + booking.nights);

      return booking.roomId === roomId && start < bookedEnd && end > bookedStart;
    });
  }
}

export default Booking;