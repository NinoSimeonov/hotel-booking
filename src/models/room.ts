export interface RoomAttributes {
    id?: number;
    singleBeds: number;
    doubleBeds: number;
    price: number;
    daysBeforeNoCancel: number;
  }
  
  class Room {
    static rooms: Room[] = [];
    static nextId: number = 1;
  
    id: number;
    singleBeds: number;
    doubleBeds: number;
    price: number;
    daysBeforeNoCancel: number;
  
    constructor({ id = Room.nextId, singleBeds, doubleBeds, price, daysBeforeNoCancel }: RoomAttributes) {
      this.id = id;
      this.singleBeds = singleBeds;
      this.doubleBeds = doubleBeds;
      this.price = price;
      this.daysBeforeNoCancel = daysBeforeNoCancel;
    }
  
    // Add the static methods for the in-memory store.
    static findAll(): Room[] {
      return this.rooms;
    }
  
    static findById(id: number): Room | undefined {
      return this.rooms.find(room => room.id === id);
    }
  
    static create(roomData: RoomAttributes): Room {
      const newRoom = new Room({ ...roomData, id: this.nextId++ });
      this.rooms.push(newRoom);
      return newRoom;
    }
  
    static updateById(id: number, updateData: Partial<RoomAttributes>): Room | undefined {
      let room = this.findById(id);
      if (!room) return undefined;
      room = { ...room, ...updateData };
      this.rooms = this.rooms.map(r => (r.id === id ? room : r));
      return room;
    }
  
    static deleteById(id: number): boolean {
      const roomIndex = this.rooms.findIndex(room => room.id === id);
      if (roomIndex === -1) return false;
      this.rooms.splice(roomIndex, 1);
      return true;
    }
  }
  
  export default Room;