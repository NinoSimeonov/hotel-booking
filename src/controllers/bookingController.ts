import { Request, Response } from 'express';
import Booking, { BookingAttributes } from '../models/booking';
import { createBookingService } from '../services/bookingService';
import { ERROR_MESSAGES } from '../utils/constants';
import { HttpStatus } from '../utils/httpStatusCodes';


export const createBooking = async (req: Request, res: Response) => {
  
  const bookingData: BookingAttributes = req.body;
  const bookingResult = await createBookingService(bookingData);

  if (typeof bookingResult === 'string') {
    res.status(HttpStatus.BAD_REQUEST).json({ message: bookingResult });
  } else {
    res.status(HttpStatus.CREATED).json(bookingResult);
  }
};

export const getBookings = async (req: Request, res: Response) => {

  const bookings = Booking.findAll();
  res.json(bookings);
};

export const getBooking = async (req: Request, res: Response) => {
  
  const bookingId = parseInt(req.params.id);
  const booking = Booking.findById(bookingId);
  if (!booking) {
    return res.status(HttpStatus.NOT_FOUND).json({ message: ERROR_MESSAGES.BOOKING_NOT_FOUND });
  }
  res.json(booking);
};

export const updateBooking = async (req: Request, res: Response) => {
  
  const bookingId = parseInt(req.params.id);
  const updateData = req.body;
  const updatedBooking = Booking.updateById(bookingId, updateData);
  if (!updatedBooking) {
    return res.status(HttpStatus.NOT_FOUND).json({ message: ERROR_MESSAGES.BOOKING_NOT_FOUND });
  }
  res.json(updatedBooking);
};

export const deleteBooking = async (req: Request, res: Response) => {
  
  const bookingId = parseInt(req.params.id);
  const result = Booking.deleteById(bookingId);
  if (result) {
    res.status(HttpStatus.CREATED).send();
  } else {
    res.status(HttpStatus.NOT_FOUND).json({ message: ERROR_MESSAGES.BOOKING_NOT_FOUND });
  }
};