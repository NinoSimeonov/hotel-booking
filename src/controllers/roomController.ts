import { Request, Response } from 'express';
import Room from '../models/room';
import * as RoomService from '../services/roomService';
import { findBestDealRoom, getAvailableRoomsService, getFilteredRoomsService, getRoomsService } from '../services/roomService';
import { ERROR_MESSAGES } from '../utils/constants';
import { HttpStatus } from '../utils/httpStatusCodes';


export const createRoom = async (req: Request, res: Response) => {

  const roomData = req.body;
  const newRoom = Room.create(roomData);
  res.status(HttpStatus.CREATED).json(newRoom);
};

export const getRooms = async (req: Request, res: Response) => {
  // Check if there are any query parameters for filtering
  if (Object.keys(req.query).length) {
    // Handle the filtered rooms
    const filteredRooms = await getFilteredRoomsService(req.query);
    res.json(filteredRooms);
  } else {
    // Handle the normal list
    const rooms = await getRoomsService();
    res.json(rooms);
  }
};

export const getRoom = async (req: Request, res: Response) => {
  
  const roomId = parseInt(req.params.id);
  const room = Room.findById(roomId);
  if (!room) {
    return res.status(HttpStatus.NOT_FOUND).json({ message: ERROR_MESSAGES.ROOM_NOT_FOUND });
  }
  res.json(room);
};

export const updateRoom = async (req: Request, res: Response) => {
  
  const roomId = parseInt(req.params.id);
  const updateData = req.body;
  const updatedRoom = Room.updateById(roomId, updateData);
  if (!updatedRoom) {
    return res.status(HttpStatus.NOT_FOUND).json({ message: ERROR_MESSAGES.ROOM_NOT_FOUND });
  }
  res.json(updatedRoom);
};

export const deleteRoom = async (req: Request, res: Response) => {
  
  const roomId = parseInt(req.params.id);
  const result = Room.deleteById(roomId);
  if (result) {
    res.status(HttpStatus.NO_CONTENT).send();
  } else {
    res.status(HttpStatus.NOT_FOUND).json({ message: ERROR_MESSAGES.ROOM_NOT_FOUND });
  }
};

export const getAvailableRooms = async (req: Request, res: Response) => {
  try {
    const startDate = req.query.startDate as string || new Date().toISOString();
    const endDate = req.query.endDate as string || '2030-12-31T23:59:59.999Z';
    const availableRooms = await getAvailableRoomsService(startDate, endDate);
    res.status(200).json(availableRooms);
  } catch (error) {
    res.status(500).json({ message: ERROR_MESSAGES.UNEXPECTED_ERROR, error });
  }
}

// The "lucky" endpoint
export const getLuckyRoom = async (req: Request, res: Response) => {
  const { date, nights, guests } = req.query;

  try {
    const bestDealRoom = await findBestDealRoom(date as string, parseInt(nights as string), parseInt(guests as string));
    
    if (!bestDealRoom) {
      return res.status(HttpStatus.NOT_FOUND).json({ message: ERROR_MESSAGES.NO_AVAILABLE_ROOMS_FOUND_FOR_THE_CRITERIA });
    }

    res.json(bestDealRoom);
  } catch (error) {
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: ERROR_MESSAGES.UNEXPECTED_ERROR, error });
  }
};

export const getFilteredRooms = async (req: Request, res: Response) => {
  const filters = req.query; // This contains all the filter query parameters
  try {
    const filteredRooms = await getFilteredRoomsService(filters);
    res.json(filteredRooms);
  } catch (error) {
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: ERROR_MESSAGES.UNEXPECTED_ERROR, error });
  }
};