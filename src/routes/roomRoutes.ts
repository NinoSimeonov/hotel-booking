import { Router } from 'express';
import * as RoomsController from '../controllers/roomController';
import { validateRoom } from '../middlewares/validateRoom';

const router = Router();

router.post('/', validateRoom, RoomsController.createRoom);

router.get('/lucky', RoomsController.getLuckyRoom);
router.get('/available', RoomsController.getAvailableRooms);
router.get('/', RoomsController.getRooms);
router.get('/', RoomsController.getFilteredRooms);
router.get('/:id', RoomsController.getRoom); // This needs to be last among the GETs

router.put('/:id', validateRoom, RoomsController.updateRoom);

router.delete('/:id', RoomsController.deleteRoom);

export default router;