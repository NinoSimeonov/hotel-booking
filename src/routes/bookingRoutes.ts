import { Router } from 'express';
import * as BookingsController from '../controllers/bookingController';
import { validateBooking } from '../middlewares/validateBooking';

const router = Router();

router.post('/', validateBooking, BookingsController.createBooking);
router.get('/', BookingsController.getBookings);
router.get('/:id', BookingsController.getBooking);
router.put('/:id', validateBooking, BookingsController.updateBooking); 
router.delete('/:id', BookingsController.deleteBooking); 

export default router;