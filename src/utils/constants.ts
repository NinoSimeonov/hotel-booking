export const ERROR_MESSAGES = {
    ROOM_NOT_FOUND: "Room does not exist",
    BOOKING_NOT_FOUND: "Booking not found",
    INSUFFICIENT_BEDS: "Insufficient beds for the number of guests. Available beds: ",
    ROOM_NOT_AVAILABLE: "Room is not available for the selected dates",
    UNABLE_TO_CREATE_BOOKING: "Unable to create booking",
    UNEXPECTED_ERROR: "An unexpected error occurred",
    ACCESS_DENIED: "Access Denied: Incorrect credentials.",
    UNABLE_TO_UPDATE_BOOKING: "Unable to update booking",
    NO_AVAILABLE_ROOMS_FOUND_FOR_THE_CRITERIA: "No available rooms found for the criteria.",
  };
  