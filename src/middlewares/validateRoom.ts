import { Request, Response, NextFunction } from 'express';
import Joi from 'joi';
import { HttpStatus } from '../utils/httpStatusCodes';

const roomSchema = Joi.object({
  singleBeds: Joi.number().integer().positive().required(),
  doubleBeds: Joi.number().integer().positive().required(),
  price: Joi.number().integer().positive().required(),
  daysBeforeNoCancel: Joi.number().integer().positive().required()
});

export const validateRoom = (req: Request, res: Response, next: NextFunction) => {
  const { error } = roomSchema.validate(req.body);
  if (error) {
    return res.status(HttpStatus.BAD_REQUEST).json(error);
  }
  next();
};
