import { Request, Response, NextFunction } from 'express';
import Joi from 'joi';
import { HttpStatus } from '../utils/httpStatusCodes';

const bookingSchema = Joi.object({
    roomId: Joi.number().integer().positive().required(),
    date: Joi.date().iso().messages({
      'date.base': `"date" should be in ISO 8601 format`,
      'date.isoDate': `"date" should be in ISO 8601 format`
    }).required(),
    guests: Joi.number().integer().min(1).required(),
    nights: Joi.number().integer().min(1).required()
  });

export const validateBooking = (req: Request, res: Response, next: NextFunction) => {
  const { error } = bookingSchema.validate(req.body);
  if (error) {
    return res.status(HttpStatus.BAD_REQUEST).json(error);
  }
  next();
};
