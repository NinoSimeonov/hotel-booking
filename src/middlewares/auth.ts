import { Request, Response, NextFunction } from 'express';
import crypto from 'crypto';
import { ERROR_MESSAGES } from '../utils/constants';
import { HttpStatus } from '../utils/httpStatusCodes';

// Simple in-memory store for users (normally you would use a database)
const users: { username: string; hash: string }[] = [];

// Utility function to hash passwords
const hashPassword = (password: string): string => {
  return crypto.createHash('sha256').update(password).digest('hex');
};

// Function to register a new user (for demonstration purposes)
export const registerUser = (username: string, password: string): void => {
  const hash = hashPassword(password);
  users.push({ username, hash });
};

// Basic Authentication Middleware
export const basicAuth = (req: Request, res: Response, next: NextFunction) => {
  const authHeader = req.headers.authorization;

  if (authHeader && authHeader.startsWith('Basic ')) {
    const base64Credentials = authHeader.slice(6);
    const credentials = Buffer.from(base64Credentials, 'base64').toString('ascii');
    const [username, password] = credentials.split(':');
    const hash = hashPassword(password);

    const user = users.find(u => u.username === username && u.hash === hash);

    if (user) {
      return next();
    }
  }

  res.status(HttpStatus.UNAUTHORIZED).json({ message: ERROR_MESSAGES.ACCESS_DENIED });
};

// Add a couple of users for testing (in a real app, use a registration process)
registerUser('admin', 'admin');
registerUser('user1', 'password123');
registerUser('user2', 'password456');

export default basicAuth;