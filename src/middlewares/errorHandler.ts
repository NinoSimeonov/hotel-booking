import { Request, Response, NextFunction } from 'express';
import { ERROR_MESSAGES } from '../utils/constants';
import { HttpStatus } from '../utils/httpStatusCodes';

export const globalErrorHandler = (err: any, req: Request, res: Response, next: NextFunction) => {
  console.error(err.stack);
  res.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ message: err.message || ERROR_MESSAGES.UNEXPECTED_ERROR });
}