import express from 'express';
import basicAuth from './middlewares/auth'
import roomsRoutes from './routes/roomRoutes';
import bookingsRoutes from './routes/bookingRoutes';
import { globalErrorHandler } from './middlewares/errorHandler';


const app = express();
const port = 3000;

app.use(express.json());
//app.use(basicAuth);
app.use('/rooms', basicAuth, roomsRoutes);
app.use('/bookings', basicAuth, bookingsRoutes);
app.use(globalErrorHandler);

app.listen(port, () => {
  console.log(`Server running on http://localhost:${port}`);
});